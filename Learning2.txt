Just Some Code from sencha docs.

//edit by Fahad

Ext.define('App.view.broken.BrokenModel', {
    extend: 'Ext.app.ViewModel',

    formulas: {
        bar: function (get) {
            return get('foo') / 2;
        },
        foo: function (get) {
            return get('bar') * 2;
        }
    }
});